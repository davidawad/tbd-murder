# Usage: python murder_tracker.py <optionalparams>
# Usage: python murder_tracker.py

import warnings
warnings.filterwarnings('ignore', category=DeprecationWarning)

from BitTornado.BT1.track import track
from sys import argv

if __name__ == '__main__':
    args = ["--dfile", "data",
            "--port", "8998"] + argv[1:]
    track(args)
